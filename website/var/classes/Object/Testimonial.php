<?php 

/** Generated at 2015-09-03T07:21:08+02:00 */

/**
* Inheritance: no
* Variants   : no
* Changed by : admin (2)
* IP:          192.168.56.1
*/


namespace Pimcore\Model\Object;



/**
* @method static \Pimcore\Model\Object\Testimonial getByAvatar ($value, $limit = 0) 
* @method static \Pimcore\Model\Object\Testimonial getByEmail ($value, $limit = 0) 
* @method static \Pimcore\Model\Object\Testimonial getByFirstname ($value, $limit = 0) 
* @method static \Pimcore\Model\Object\Testimonial getByLastname ($value, $limit = 0) 
* @method static \Pimcore\Model\Object\Testimonial getByTestimonial ($value, $limit = 0) 
*/

class Testimonial extends Concrete {

public $o_classId = 3;
public $o_className = "Testimonial";
public $avatar;
public $email;
public $firstname;
public $lastname;
public $testimonial;


/**
* @param array $values
* @return \Pimcore\Model\Object\Testimonial
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get avatar - Avatar
* @return \Pimcore\Model\Asset\Image
*/
public function getAvatar () {
	$preValue = $this->preGetValue("avatar"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->avatar;
	return $data;
}

/**
* Set avatar - Avatar
* @param \Pimcore\Model\Asset\Image $avatar
* @return \Pimcore\Model\Object\Testimonial
*/
public function setAvatar ($avatar) {
	$this->avatar = $avatar;
	return $this;
}

/**
* Get email - Email
* @return string
*/
public function getEmail () {
	$preValue = $this->preGetValue("email"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->email;
	return $data;
}

/**
* Set email - Email
* @param string $email
* @return \Pimcore\Model\Object\Testimonial
*/
public function setEmail ($email) {
	$this->email = $email;
	return $this;
}

/**
* Get firstname - Firstname
* @return string
*/
public function getFirstname () {
	$preValue = $this->preGetValue("firstname"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->firstname;
	return $data;
}

/**
* Set firstname - Firstname
* @param string $firstname
* @return \Pimcore\Model\Object\Testimonial
*/
public function setFirstname ($firstname) {
	$this->firstname = $firstname;
	return $this;
}

/**
* Get lastname - Lastname
* @return string
*/
public function getLastname () {
	$preValue = $this->preGetValue("lastname"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->lastname;
	return $data;
}

/**
* Set lastname - Lastname
* @param string $lastname
* @return \Pimcore\Model\Object\Testimonial
*/
public function setLastname ($lastname) {
	$this->lastname = $lastname;
	return $this;
}

/**
* Get testimonial - Testimonial
* @return string
*/
public function getTestimonial () {
	$preValue = $this->preGetValue("testimonial"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->testimonial;
	return $data;
}

/**
* Set testimonial - Testimonial
* @param string $testimonial
* @return \Pimcore\Model\Object\Testimonial
*/
public function setTestimonial ($testimonial) {
	$this->testimonial = $testimonial;
	return $this;
}

protected static $_relationFields = array (
);

public $lazyLoadedFields = NULL;

}

