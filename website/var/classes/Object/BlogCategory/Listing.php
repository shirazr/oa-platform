<?php 

namespace Pimcore\Model\Object\BlogCategory;

use Pimcore\Model\Object;

/**
 * @method Object\BlogCategory current()
 */

class Listing extends Object\Listing\Concrete {

public $classId = 2;
public $className = "blogCategory";


}
