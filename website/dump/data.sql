
SET NAMES UTF8;



DROP TABLE IF EXISTS `object_localized_data_1`;
CREATE TABLE `object_localized_data_1` (
  `ooo_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `text` longtext,
  `tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ooo_id`,`language`),
  KEY `ooo_id` (`ooo_id`),
  KEY `language` (`language`),
  KEY `p_index_tags` (`tags`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_localized_data_2`;
CREATE TABLE `object_localized_data_2` (
  `ooo_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ooo_id`,`language`),
  KEY `ooo_id` (`ooo_id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_localized_query_1_en`;
CREATE TABLE `object_localized_query_1_en` (
  `ooo_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `text` longtext,
  `tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ooo_id`,`language`),
  KEY `ooo_id` (`ooo_id`),
  KEY `language` (`language`),
  KEY `p_index_tags` (`tags`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_localized_query_2_en`;
CREATE TABLE `object_localized_query_2_en` (
  `ooo_id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ooo_id`,`language`),
  KEY `ooo_id` (`ooo_id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_query_1`;
CREATE TABLE `object_query_1` (
  `oo_id` int(11) NOT NULL DEFAULT '0',
  `oo_classId` int(11) DEFAULT '1',
  `oo_className` varchar(255) DEFAULT 'blogPost',
  `date` bigint(20) DEFAULT NULL,
  `categories` text,
  `featuredImage__image` int(11) DEFAULT NULL,
  `featuredImage__hotspots` text,
  PRIMARY KEY (`oo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_query_2`;
CREATE TABLE `object_query_2` (
  `oo_id` int(11) NOT NULL DEFAULT '0',
  `oo_classId` int(11) DEFAULT '2',
  `oo_className` varchar(255) DEFAULT 'blogCategory',
  PRIMARY KEY (`oo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_query_3`;
CREATE TABLE `object_query_3` (
  `oo_id` int(11) NOT NULL DEFAULT '0',
  `oo_classId` int(11) DEFAULT '3',
  `oo_className` varchar(255) DEFAULT 'Testimonial',
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `avatar` int(11) DEFAULT NULL,
  `testimonial` longtext,
  PRIMARY KEY (`oo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_relations_1`;
CREATE TABLE `object_relations_1` (
  `src_id` int(11) NOT NULL DEFAULT '0',
  `dest_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '',
  `fieldname` varchar(70) NOT NULL DEFAULT '0',
  `index` int(11) unsigned NOT NULL DEFAULT '0',
  `ownertype` enum('object','fieldcollection','localizedfield','objectbrick') NOT NULL DEFAULT 'object',
  `ownername` varchar(70) NOT NULL DEFAULT '',
  `position` varchar(70) NOT NULL DEFAULT '0',
  PRIMARY KEY (`src_id`,`dest_id`,`ownertype`,`ownername`,`fieldname`,`type`,`position`),
  KEY `index` (`index`),
  KEY `src_id` (`src_id`),
  KEY `dest_id` (`dest_id`),
  KEY `fieldname` (`fieldname`),
  KEY `position` (`position`),
  KEY `ownertype` (`ownertype`),
  KEY `type` (`type`),
  KEY `ownername` (`ownername`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_relations_2`;
CREATE TABLE `object_relations_2` (
  `src_id` int(11) NOT NULL DEFAULT '0',
  `dest_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '',
  `fieldname` varchar(70) NOT NULL DEFAULT '0',
  `index` int(11) unsigned NOT NULL DEFAULT '0',
  `ownertype` enum('object','fieldcollection','localizedfield','objectbrick') NOT NULL DEFAULT 'object',
  `ownername` varchar(70) NOT NULL DEFAULT '',
  `position` varchar(70) NOT NULL DEFAULT '0',
  PRIMARY KEY (`src_id`,`dest_id`,`ownertype`,`ownername`,`fieldname`,`type`,`position`),
  KEY `index` (`index`),
  KEY `src_id` (`src_id`),
  KEY `dest_id` (`dest_id`),
  KEY `fieldname` (`fieldname`),
  KEY `position` (`position`),
  KEY `ownertype` (`ownertype`),
  KEY `type` (`type`),
  KEY `ownername` (`ownername`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_relations_3`;
CREATE TABLE `object_relations_3` (
  `src_id` int(11) NOT NULL DEFAULT '0',
  `dest_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL DEFAULT '',
  `fieldname` varchar(70) NOT NULL DEFAULT '0',
  `index` int(11) unsigned NOT NULL DEFAULT '0',
  `ownertype` enum('object','fieldcollection','localizedfield','objectbrick') NOT NULL DEFAULT 'object',
  `ownername` varchar(70) NOT NULL DEFAULT '',
  `position` varchar(70) NOT NULL DEFAULT '0',
  PRIMARY KEY (`src_id`,`dest_id`,`ownertype`,`ownername`,`fieldname`,`type`,`position`),
  KEY `index` (`index`),
  KEY `src_id` (`src_id`),
  KEY `dest_id` (`dest_id`),
  KEY `fieldname` (`fieldname`),
  KEY `position` (`position`),
  KEY `ownertype` (`ownertype`),
  KEY `type` (`type`),
  KEY `ownername` (`ownername`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_store_1`;
CREATE TABLE `object_store_1` (
  `oo_id` int(11) NOT NULL DEFAULT '0',
  `date` bigint(20) DEFAULT NULL,
  `featuredImage__image` int(11) DEFAULT NULL,
  `featuredImage__hotspots` text,
  PRIMARY KEY (`oo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_store_2`;
CREATE TABLE `object_store_2` (
  `oo_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`oo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `object_store_3`;
CREATE TABLE `object_store_3` (
  `oo_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `avatar` int(11) DEFAULT NULL,
  `testimonial` longtext,
  PRIMARY KEY (`oo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





INSERT INTO `assets` VALUES ('1','0','folder','','/',NULL,'1426146515','1426146515','1','1',NULL,'0');
INSERT INTO `assets` VALUES ('3','1','folder','blog','/',NULL,'1426162282','1426162282','2','2','a:0:{}','0');
INSERT INTO `assets` VALUES ('4','3','folder','media-library','/blog/',NULL,'1426162294','1426162294','2','2','a:0:{}','0');
INSERT INTO `assets` VALUES ('6','4','image','381121-1440x900.jpg','/blog/media-library/','image/jpeg','1426162427','1426162427','2','2','a:3:{s:10:\"imageWidth\";i:1440;s:11:\"imageHeight\";i:900;s:25:\"imageDimensionsCalculated\";b:1;}','0');
INSERT INTO `assets` VALUES ('7','1','folder','testimonials','/',NULL,'1441257648','1441257648','2','2','a:0:{}','0');












INSERT INTO `classes` VALUES ('1','blogPost','','1426161819','1426162307','2','2','0','0','','','','','a:2:{s:4:\"grid\";a:5:{s:2:\"id\";b:1;s:4:\"path\";b:1;s:9:\"published\";b:1;s:16:\"modificationDate\";b:1;s:12:\"creationDate\";b:1;}s:6:\"search\";a:5:{s:2:\"id\";b:1;s:4:\"path\";b:1;s:9:\"published\";b:1;s:16:\"modificationDate\";b:1;s:12:\"creationDate\";b:1;}}','0');
INSERT INTO `classes` VALUES ('2','blogCategory','Blog Category','1426161832','1426162183','2','2','0','0','','','','','a:2:{s:4:\"grid\";a:5:{s:2:\"id\";b:1;s:4:\"path\";b:1;s:9:\"published\";b:1;s:16:\"modificationDate\";b:1;s:12:\"creationDate\";b:1;}s:6:\"search\";a:5:{s:2:\"id\";b:1;s:4:\"path\";b:1;s:9:\"published\";b:1;s:16:\"modificationDate\";b:1;s:12:\"creationDate\";b:1;}}','0');
INSERT INTO `classes` VALUES ('3','Testimonial','','1441257438','1441257668','2','2','0','0','','','','','a:2:{s:4:\"grid\";a:5:{s:2:\"id\";b:1;s:4:\"path\";b:1;s:9:\"published\";b:1;s:16:\"modificationDate\";b:1;s:12:\"creationDate\";b:1;}s:6:\"search\";a:5:{s:2:\"id\";b:1;s:4:\"path\";b:1;s:9:\"published\";b:1;s:16:\"modificationDate\";b:1;s:12:\"creationDate\";b:1;}}','0');








INSERT INTO `dependencies` VALUES ('document','2','document','1');
INSERT INTO `dependencies` VALUES ('object','5','asset','6');
INSERT INTO `dependencies` VALUES ('object','5','object','6');




INSERT INTO `documents` VALUES ('1','0','page','','/','999999','1','1426146515','1426146515','1','1');
INSERT INTO `documents` VALUES ('2','1','link','en','/','1','1','1426161996','1426162055','2','2');
INSERT INTO `documents` VALUES ('3','1','folder','shared','/','2','1','1426162026','1426162041','2','2');
INSERT INTO `documents` VALUES ('4','3','folder','includes','/shared/','1','1','1426162033','1426162033','2','2');
INSERT INTO `documents` VALUES ('5','1','page','test','/','3','1','1438763762','1438763770','4','4');




















INSERT INTO `documents_link` VALUES ('2','document','1','','internal');




INSERT INTO `documents_page` VALUES ('1',NULL,'','','','','','',NULL,NULL,NULL,NULL,NULL);
INSERT INTO `documents_page` VALUES ('5','','test','default',NULL,'Test','','','a:0:{}',NULL,NULL,'','');




































INSERT INTO `object_localized_data_1` VALUES ('5','en','Sample Post','<p>This is a test Article</p>\n\n<p><img height=\"101\" pimcore_id=\"6\" pimcore_type=\"asset\" src=\"/website/var/tmp/image-thumbnails/0/6/thumb__auto_f2037a6af4d885f336eaa8c9b0bf0ff7/381121-1440x900.jpeg\" width=\"162\" /></p>\n\n<p>This is another test article</p>\n',NULL);




INSERT INTO `object_localized_data_2` VALUES ('6','en','Article');




INSERT INTO `object_localized_query_1_en` VALUES ('5','en','Sample Post','This is a test Article <img height=\"101\" pimcore_id=\"6\" pimcore_type=\"asset\" src=\"/website/var/tmp/image-thumbnails/0/6/thumb__auto_f2037a6af4d885f336eaa8c9b0bf0ff7/381121-1440x900.jpeg\" width=\"162\" /> This is another test article ',NULL);




INSERT INTO `object_localized_query_2_en` VALUES ('6','en','Article');




INSERT INTO `object_query_1` VALUES ('5','1','blogPost','1426162200',',6,','6','a:3:{s:8:\"hotspots\";a:0:{}s:6:\"marker\";a:0:{}s:4:\"crop\";N;}');




INSERT INTO `object_query_2` VALUES ('6','2','blogCategory');




INSERT INTO `object_query_3` VALUES ('8','3','Testimonial',NULL,NULL,NULL,NULL,NULL);




INSERT INTO `object_relations_1` VALUES ('5','6','object','categories','1','object','','0');












INSERT INTO `object_store_1` VALUES ('5','1426162200','6','a:3:{s:8:\"hotspots\";a:0:{}s:6:\"marker\";a:0:{}s:4:\"crop\";N;}');




INSERT INTO `object_store_2` VALUES ('6');




INSERT INTO `object_store_3` VALUES ('8',NULL,NULL,NULL,NULL,NULL);




INSERT INTO `objects` VALUES ('1','0','folder','','/','999999','1','1426146515','1426146515','1','1',NULL,NULL);
INSERT INTO `objects` VALUES ('2','1','folder','blog','/',NULL,'1','1426162076','1426162076','2','2',NULL,NULL);
INSERT INTO `objects` VALUES ('3','2','folder','posts','/blog/',NULL,'1','1426162083','1426162083','2','2',NULL,NULL);
INSERT INTO `objects` VALUES ('4','2','folder','categories','/blog/',NULL,'1','1426162090','1426162090','2','2',NULL,NULL);
INSERT INTO `objects` VALUES ('5','3','object','sample-post','/blog/posts/','0','1','1426162219','1426162819','2','2','1','blogPost');
INSERT INTO `objects` VALUES ('6','4','object','article','/blog/categories/','0','1','1426162575','1426162584','2','2','2','blogCategory');
INSERT INTO `objects` VALUES ('7','1','folder','testimonials','/',NULL,'1','1441257573','1441257573','2','2',NULL,NULL);
INSERT INTO `objects` VALUES ('8','7','object','default','/testimonials/',NULL,'0','1441257585','1441257585','2','2','3','Testimonial');




INSERT INTO `properties` VALUES ('5','document','/test','navigation_name','text','test','1');








INSERT INTO `recyclebin` VALUES ('1','asset','image','/2560x1440-copy-.jpg','1','1426162312','admin');
INSERT INTO `recyclebin` VALUES ('2','asset','image','/381121-1440x900.jpg','1','1426162398','admin');








INSERT INTO `sanitycheck` VALUES ('5','object');








INSERT INTO `search_backend_data` VALUES ('2','/en','document','link','link','1','1426161996','1426162055','2','2','ID: 2  \nPath: /en  \n /','');
INSERT INTO `search_backend_data` VALUES ('2','/blog','object','folder','folder','1','1426162076','1426162076','2','2','ID: 2  \nPath: /blog  \nblog','');
INSERT INTO `search_backend_data` VALUES ('3','/blog','asset','folder','folder','1','1426162282','1426162282','2','2','ID: 3  \nPath: /blog  \nblog','');
INSERT INTO `search_backend_data` VALUES ('3','/shared','document','folder','folder','1','1426162026','1426162041','2','2','ID: 3  \nPath: /shared  \nshared','');
INSERT INTO `search_backend_data` VALUES ('3','/blog/posts','object','folder','folder','1','1426162083','1426162083','2','2','ID: 3  \nPath: /blog/posts  \nposts','');
INSERT INTO `search_backend_data` VALUES ('4','/blog/media-library','asset','folder','folder','1','1426162294','1426162294','2','2','ID: 4  \nPath: /blog/media-library  \nmedia-library','');
INSERT INTO `search_backend_data` VALUES ('4','/shared/includes','document','folder','folder','1','1426162033','1426162033','2','2','ID: 4  \nPath: /shared/includes  \nincludes','');
INSERT INTO `search_backend_data` VALUES ('4','/blog/categories','object','folder','folder','1','1426162090','1426162090','2','2','ID: 4  \nPath: /blog/categories  \ncategories','');
INSERT INTO `search_backend_data` VALUES ('5','/test','document','page','page','1','1438763762','1438763770','4','4','ID: 5  \nPath: /test  \n Test ','navigation_name:test ');
INSERT INTO `search_backend_data` VALUES ('5','/blog/posts/sample-post','object','object','blogPost','1','1426162219','1426162819','2','2','ID: 5  \nPath: /blog/posts/sample-post  \nSample Post This is a test Article This is another test article Mar 12, 2015 1:10:00 PM /blog/categories/article TzozODoiUGltY29yZVxNb2RlbFxPYmplY3RcRGF0YVxIb3RzcG90aW1hZ2UiOjQ6e3M6NToiaW1hZ2UiO086MjU6IlBpbWNvcmVcTW9kZWxcQXNzZXRcSW1hZ2UiOjE3OntzOjQ6InR5cGUiO3M6NToiaW1hZ2UiO3M6MjoiaWQiO2k6NjtzOjg6InBhcmVudElkIjtpOjQ7czo4OiJmaWxlbmFtZSI7czoxOToiMzgxMTIxLTE0NDB4OTAwLmpwZyI7czo0OiJwYXRoIjtzOjIwOiIvYmxvZy9tZWRpYS1saWJyYXJ5LyI7czo4OiJtaW1ldHlwZSI7czoxMDoiaW1hZ2UvanBlZyI7czoxMjoiY3JlYXRpb25EYXRlIjtpOjE0MjYxNjI0Mjc7czoxNjoibW9kaWZpY2F0aW9uRGF0ZSI7aToxNDI2MTYyNDI3O3M6OToidXNlck93bmVyIjtzOjE6IjIiO3M6MTY6InVzZXJNb2RpZmljYXRpb24iO3M6MToiMiI7czo4OiJtZXRhZGF0YSI7YTowOnt9czo2OiJsb2NrZWQiO047czoxNDoiY3VzdG9tU2V0dGluZ3MiO2E6Mzp7czoxMDoiaW1hZ2VXaWR0aCI7aToxNDQwO3M6MTE6ImltYWdlSGVpZ2h0IjtpOjkwMDtzOjI1OiJpbWFnZURpbWVuc2lvbnNDYWxjdWxhdGVkIjtiOjE7fXM6ODoic2libGluZ3MiO047czoxMToiaGFzU2libGluZ3MiO047czoxNToiACoAX2RhdGFDaGFuZ2VkIjtiOjA7czoyNDoiX19fX3BpbWNvcmVfY2FjaGVfaXRlbV9fIjtzOjE1OiJwaW1jb3JlX2Fzc2V0XzYiO31zOjg6ImhvdHNwb3RzIjthOjA6e31zOjY6Im1hcmtlciI7YTowOnt9czo0OiJjcm9wIjtOO30= ','');
INSERT INTO `search_backend_data` VALUES ('6','/blog/media-library/381121-1440x900.jpg','asset','image','image','1','1426162427','1426162427','2','2','ID: 6  \nPath: /blog/media-library/381121-1440x900.jpg  \n381121-1440x900.jpg','');
INSERT INTO `search_backend_data` VALUES ('6','/blog/categories/article','object','object','blogCategory','1','1426162575','1426162584','2','2','ID: 6  \nPath: /blog/categories/article  \nArticle ','');
INSERT INTO `search_backend_data` VALUES ('7','/testimonials','asset','folder','folder','1','1441257648','1441257648','2','2','ID: 7  \nPath: /testimonials  \ntestimonials','');
INSERT INTO `search_backend_data` VALUES ('7','/testimonials','object','folder','folder','1','1441257573','1441257573','2','2','ID: 7  \nPath: /testimonials  \ntestimonials','');
INSERT INTO `search_backend_data` VALUES ('8','/testimonials/default','object','object','Testimonial','0','1441257585','1441257585','2','2','ID: 8  \nPath: /testimonials/default  \n ','');




















INSERT INTO `translations_admin` VALUES ('avatar','en','','1441257597','1441257597');
INSERT INTO `translations_admin` VALUES ('blogcategory','en','','1426161838','1426161838');
INSERT INTO `translations_admin` VALUES ('blogpost','en','','1426161838','1426161838');
INSERT INTO `translations_admin` VALUES ('categories','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('date','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('email','en','','1441257597','1441257597');
INSERT INTO `translations_admin` VALUES ('featured image','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('firstname','en','','1441257597','1441257597');
INSERT INTO `translations_admin` VALUES ('lastname','en','','1441257597','1441257597');
INSERT INTO `translations_admin` VALUES ('master','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('master (admin mode)','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('name','en','','1426162582','1426162582');
INSERT INTO `translations_admin` VALUES ('tags','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('testimonial','en','','1441257446','1441257446');
INSERT INTO `translations_admin` VALUES ('text','en','','1426162229','1426162229');
INSERT INTO `translations_admin` VALUES ('title','en','','1426162229','1426162229');








INSERT INTO `tree_locks` VALUES ('2','document','self');
INSERT INTO `tree_locks` VALUES ('3','document','propagate');




INSERT INTO `users` VALUES ('0','0','user','system',NULL,NULL,NULL,NULL,NULL,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO `users` VALUES ('2','0','user','admin','$2y$10$QzWwYzfmNF6ZtxudgAQJcefz3kAa/enQHxGqR1SGWTcQIIBzfu16.',NULL,NULL,NULL,'en','1','1','','','1','1','1','','',NULL);
INSERT INTO `users` VALUES ('4','0','user','SuperAdmin','$2y$10$LcBc8jbbXD0QquDDA3II5OIryNu6iDemRhNXEB./mAM52RHyZLC8W',NULL,NULL,NULL,'en','1','1','','','1','1','1','','',NULL);




INSERT INTO `users_permission_definitions` VALUES ('assets');
INSERT INTO `users_permission_definitions` VALUES ('backup');
INSERT INTO `users_permission_definitions` VALUES ('classes');
INSERT INTO `users_permission_definitions` VALUES ('clear_cache');
INSERT INTO `users_permission_definitions` VALUES ('clear_temp_files');
INSERT INTO `users_permission_definitions` VALUES ('dashboards');
INSERT INTO `users_permission_definitions` VALUES ('documents');
INSERT INTO `users_permission_definitions` VALUES ('document_style_editor');
INSERT INTO `users_permission_definitions` VALUES ('document_types');
INSERT INTO `users_permission_definitions` VALUES ('emails');
INSERT INTO `users_permission_definitions` VALUES ('glossary');
INSERT INTO `users_permission_definitions` VALUES ('http_errors');
INSERT INTO `users_permission_definitions` VALUES ('newsletter');
INSERT INTO `users_permission_definitions` VALUES ('notes_events');
INSERT INTO `users_permission_definitions` VALUES ('objects');
INSERT INTO `users_permission_definitions` VALUES ('plugins');
INSERT INTO `users_permission_definitions` VALUES ('predefined_properties');
INSERT INTO `users_permission_definitions` VALUES ('qr_codes');
INSERT INTO `users_permission_definitions` VALUES ('recyclebin');
INSERT INTO `users_permission_definitions` VALUES ('redirects');
INSERT INTO `users_permission_definitions` VALUES ('reports');
INSERT INTO `users_permission_definitions` VALUES ('robots.txt');
INSERT INTO `users_permission_definitions` VALUES ('routes');
INSERT INTO `users_permission_definitions` VALUES ('seemode');
INSERT INTO `users_permission_definitions` VALUES ('sent_emails');
INSERT INTO `users_permission_definitions` VALUES ('seo_document_editor');
INSERT INTO `users_permission_definitions` VALUES ('system_settings');
INSERT INTO `users_permission_definitions` VALUES ('tag_snippet_management');
INSERT INTO `users_permission_definitions` VALUES ('targeting');
INSERT INTO `users_permission_definitions` VALUES ('thumbnails');
INSERT INTO `users_permission_definitions` VALUES ('translations');
INSERT INTO `users_permission_definitions` VALUES ('users');
INSERT INTO `users_permission_definitions` VALUES ('website_settings');




















INSERT INTO `website_settings` VALUES ('1','wistiaKey','text','378aaeb9e7f7afdb0b27a14824f5abca04ee4d27f5cda6e9b40affaf0ed4cd8c','0','1438763974','1438764409');
INSERT INTO `website_settings` VALUES ('2','grooveHq','text','f3233e3420183e611e031157fa77c36fb6535a05d71a3eda2df4aa35337b9bd0','0','1438773565','1438773592');
INSERT INTO `website_settings` VALUES ('3','infusionsoft','text','ru240|||907a123dcb7591dddb213e555c9b6823|||http://overcome.tv','0','1438778908','1438778967');






DROP VIEW IF EXISTS `object_1`;
CREATE ALGORITHM=UNDEFINED  VIEW `object_1` AS select `object_query_1`.`oo_id` AS `oo_id`,`object_query_1`.`oo_classId` AS `oo_classId`,`object_query_1`.`oo_className` AS `oo_className`,`object_query_1`.`date` AS `date`,`object_query_1`.`categories` AS `categories`,`object_query_1`.`featuredImage__image` AS `featuredImage__image`,`object_query_1`.`featuredImage__hotspots` AS `featuredImage__hotspots`,`objects`.`o_id` AS `o_id`,`objects`.`o_parentId` AS `o_parentId`,`objects`.`o_type` AS `o_type`,`objects`.`o_key` AS `o_key`,`objects`.`o_path` AS `o_path`,`objects`.`o_index` AS `o_index`,`objects`.`o_published` AS `o_published`,`objects`.`o_creationDate` AS `o_creationDate`,`objects`.`o_modificationDate` AS `o_modificationDate`,`objects`.`o_userOwner` AS `o_userOwner`,`objects`.`o_userModification` AS `o_userModification`,`objects`.`o_classId` AS `o_classId`,`objects`.`o_className` AS `o_className` from (`object_query_1` join `objects` on((`objects`.`o_id` = `object_query_1`.`oo_id`)));





DROP VIEW IF EXISTS `object_2`;
CREATE ALGORITHM=UNDEFINED  VIEW `object_2` AS select `object_query_2`.`oo_id` AS `oo_id`,`object_query_2`.`oo_classId` AS `oo_classId`,`object_query_2`.`oo_className` AS `oo_className`,`objects`.`o_id` AS `o_id`,`objects`.`o_parentId` AS `o_parentId`,`objects`.`o_type` AS `o_type`,`objects`.`o_key` AS `o_key`,`objects`.`o_path` AS `o_path`,`objects`.`o_index` AS `o_index`,`objects`.`o_published` AS `o_published`,`objects`.`o_creationDate` AS `o_creationDate`,`objects`.`o_modificationDate` AS `o_modificationDate`,`objects`.`o_userOwner` AS `o_userOwner`,`objects`.`o_userModification` AS `o_userModification`,`objects`.`o_classId` AS `o_classId`,`objects`.`o_className` AS `o_className` from (`object_query_2` join `objects` on((`objects`.`o_id` = `object_query_2`.`oo_id`)));





DROP VIEW IF EXISTS `object_3`;
CREATE ALGORITHM=UNDEFINED  VIEW `object_3` AS select `object_query_3`.`oo_id` AS `oo_id`,`object_query_3`.`oo_classId` AS `oo_classId`,`object_query_3`.`oo_className` AS `oo_className`,`object_query_3`.`email` AS `email`,`object_query_3`.`firstname` AS `firstname`,`object_query_3`.`lastname` AS `lastname`,`object_query_3`.`avatar` AS `avatar`,`object_query_3`.`testimonial` AS `testimonial`,`objects`.`o_id` AS `o_id`,`objects`.`o_parentId` AS `o_parentId`,`objects`.`o_type` AS `o_type`,`objects`.`o_key` AS `o_key`,`objects`.`o_path` AS `o_path`,`objects`.`o_index` AS `o_index`,`objects`.`o_published` AS `o_published`,`objects`.`o_creationDate` AS `o_creationDate`,`objects`.`o_modificationDate` AS `o_modificationDate`,`objects`.`o_userOwner` AS `o_userOwner`,`objects`.`o_userModification` AS `o_userModification`,`objects`.`o_classId` AS `o_classId`,`objects`.`o_className` AS `o_className` from (`object_query_3` join `objects` on((`objects`.`o_id` = `object_query_3`.`oo_id`)));





DROP VIEW IF EXISTS `object_localized_1_en`;
CREATE ALGORITHM=UNDEFINED  VIEW `object_localized_1_en` AS select `object_query_1`.`oo_id` AS `oo_id`,`object_query_1`.`oo_classId` AS `oo_classId`,`object_query_1`.`oo_className` AS `oo_className`,`object_query_1`.`date` AS `date`,`object_query_1`.`categories` AS `categories`,`object_query_1`.`featuredImage__image` AS `featuredImage__image`,`object_query_1`.`featuredImage__hotspots` AS `featuredImage__hotspots`,`objects`.`o_id` AS `o_id`,`objects`.`o_parentId` AS `o_parentId`,`objects`.`o_type` AS `o_type`,`objects`.`o_key` AS `o_key`,`objects`.`o_path` AS `o_path`,`objects`.`o_index` AS `o_index`,`objects`.`o_published` AS `o_published`,`objects`.`o_creationDate` AS `o_creationDate`,`objects`.`o_modificationDate` AS `o_modificationDate`,`objects`.`o_userOwner` AS `o_userOwner`,`objects`.`o_userModification` AS `o_userModification`,`objects`.`o_classId` AS `o_classId`,`objects`.`o_className` AS `o_className`,`object_localized_query_1_en`.`ooo_id` AS `ooo_id`,`object_localized_query_1_en`.`language` AS `language`,`object_localized_query_1_en`.`title` AS `title`,`object_localized_query_1_en`.`text` AS `text`,`object_localized_query_1_en`.`tags` AS `tags` from ((`object_query_1` join `objects` on((`objects`.`o_id` = `object_query_1`.`oo_id`))) left join `object_localized_query_1_en` on((`object_query_1`.`oo_id` = `object_localized_query_1_en`.`ooo_id`)));





DROP VIEW IF EXISTS `object_localized_2_en`;
CREATE ALGORITHM=UNDEFINED  VIEW `object_localized_2_en` AS select `object_query_2`.`oo_id` AS `oo_id`,`object_query_2`.`oo_classId` AS `oo_classId`,`object_query_2`.`oo_className` AS `oo_className`,`objects`.`o_id` AS `o_id`,`objects`.`o_parentId` AS `o_parentId`,`objects`.`o_type` AS `o_type`,`objects`.`o_key` AS `o_key`,`objects`.`o_path` AS `o_path`,`objects`.`o_index` AS `o_index`,`objects`.`o_published` AS `o_published`,`objects`.`o_creationDate` AS `o_creationDate`,`objects`.`o_modificationDate` AS `o_modificationDate`,`objects`.`o_userOwner` AS `o_userOwner`,`objects`.`o_userModification` AS `o_userModification`,`objects`.`o_classId` AS `o_classId`,`objects`.`o_className` AS `o_className`,`object_localized_query_2_en`.`ooo_id` AS `ooo_id`,`object_localized_query_2_en`.`language` AS `language`,`object_localized_query_2_en`.`name` AS `name` from ((`object_query_2` join `objects` on((`objects`.`o_id` = `object_query_2`.`oo_id`))) left join `object_localized_query_2_en` on((`object_query_2`.`oo_id` = `object_localized_query_2_en`.`ooo_id`)));

