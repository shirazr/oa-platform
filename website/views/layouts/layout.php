<!DOCTYPE html>
<html lang="en" class="style-1">
<head>
    <?php
    if (!$this->document) {
        $this->document = Document::getById(1);
    }
    if ($this->document->getTitle()) {
        $this->headTitle()->set($this->document->getTitle());
    }
    if ($this->document->getDescription()) {
        $this->headMeta()->appendName('description', $this->document->getDescription());
    }
    //$this->headTitle()->append("Online Associates");
    //$this->headTitle()->setSeparator(" | ");
    echo $this->headTitle();
    echo $this->headMeta();
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/static/assets/fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
    <link href="/static/assets/fonts/roboto/roboto.css" rel="stylesheet">
    <link href="/static/assets/fonts/roboto-cond/roboto-cond.css" rel="stylesheet">
    <link href="/static/assets/fonts/themify/themify-icons.css" rel="stylesheet">
    <link href="/static/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/assets/css/ui.css" rel="stylesheet">
    <link href="/static/assets/css/website.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="/static/assets/js/html5shiv.js"></script>
    <script src="/static/assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-bg">
<div class="page-wrapper">
    <section class="nav-block">
        <nav class="nav-top navbar hnav hnav-sm invert-colors bcolor-bg" role="navigation">
            <div class="container">
                <div class="navbar-dont-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="#">home</a> <i class="fa fa-angle-double-right"></i> <a href="#">about us</a> <i
                                class="fa fa-angle-double-right"></i> <a href="#">technologies</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden-xs"><a href="/login">CLIENT LOGIN</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <nav class="nav-main navbar hnav" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="ti ti-menu"></i>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img class="hidden-xs" src="/static/assets/img/oa/oa-site-logo.png" alt=""/>
                        <img class="visible-xs" src="/static/assets/img/oa/logo-xs.png" alt=""/>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right case-u">
                        <li class="active"><a href="/">home</a></li>
                        <li><a href="/services">services</a></li>
                        <li><a href="/projects">projects</a></li>
                        <li><a href="/about">about us</a></li>
                        <li><a href="/careers">careers</a></li>
                        <li><a href="/contact-us">contact us</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </section>

    <?= $this->layout()->content; ?>

    <footer class="footer-block">
        <div class="bottom">
            <div class="container text-center">
                <span class="copy-text">&copy; <?= date('Y') ?> Online Associates (Pvt) Ltd</span>
            </div>
        </div>
    </footer>
</div>

<script src="/static/assets/js/jquery-latest.min.js"></script>
<script src="/static/assets/js/vendor.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip().click(function () {
            return false;
        });
    });
</script>

</body>
</html>