<?php
include_once("../pimcore/cli/startup.php");
Zend_Session::$_unitTestEnabled = true;

if (!defined("PHPUNIT_TEST")) {
    define('PHPUNIT_TEST', true);
}

if(!defined("TESTS_PATH")) {
    define('TESTS_PATH', realpath(dirname(__FILE__)));
}